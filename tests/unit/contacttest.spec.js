import { mount } from '@vue/test-utils';
import Contact from '../../src/views/Contact.vue';
import { ref } from 'vue';

test('completes a todo', async () => {
  const admin = ref(true);
  const wrapper = mount(Contact, {
    admin
  });

  // const todo = wrapper.get('[data-test="todo"]');
  // expect(todo.text()).toBe('Learn Vue.js 3');

  // -----------------------------------
  // expect(wrapper.findAll('[data-test="todo"]')).toHaveLength(1);

  // await wrapper.get('[data-test="new-todo"]').setValue('New todo');
  // await wrapper.get('[data-test="form"]').trigger('submit');

  // expect(wrapper.findAll('[data-test="todo"]')).toHaveLength(2);

  // ----------------------------------------
  // await wrapper.get('[data-test="todo-checkbox"]').setValue(true);
  // expect(wrapper.get('[data-test="todo"]').classes()).toContain('completed');

  expect(wrapper.find('#admin').exists()).toBe(true);
});
