import Vue from 'vue';
import { createStore } from 'vuex';
import { mount } from '@vue/test-utils';

import About from '@/views/About.vue';
import { state, getters, mutations } from '@/store/modules/common';

const store = createStore({
  modules: {
    common: {
      state,
      getters,
      mutations
    }
  }
});

test('About', async () => {
  const wrapper = mount(About, {
    global: {
      plugins: [store]
    }
  });

  store.state.common.count = 2;
  console.log(store.state.common.count);

  wrapper.find('button').trigger('click');
  expect(wrapper.find('#count').text()).toEqual('2');
});
