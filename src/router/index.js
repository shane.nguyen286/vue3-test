import { createRouter, createWebHistory } from 'vue-router';
import routes from './routes';
import store from '../store';
import middlewarePineline from './middlewarePineline';
import guest from './middleware/guest';

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

router.beforeEach((to, from, next) => {
  const middleware = !to.meta.middleware
    ? [guest]
    : [...to.meta.middleware, guest];

  const context = {
    to,
    from,
    next,
    store
  };

  return middleware[0]({
    ...context,
    next: middlewarePineline(context, middleware, 1)
  });
});

export default router;
