import auth from './middleware/auth';
import guest from './middleware/guest';
import isSubscriber from './middleware/isSubscriber';

function views(path) {
  return () => import(`@/views/${path}`).then((m) => m.default || m);
}

// NOTE: multilevel
// {
//   path: '',
//   component: views('Home.vue'),
//   children: [
//     {
//       path: 'home-1',
//       component: {
//         template: `<router-view></router-view>`
//       },
//       children: [
//         {
//           path: 'about',
//           component: () => import('../views/About.vue')
//         }
//       ]
//     }
//   ]
// }

export default [
  {
    path: '/',
    name: 'Home',
    component: () => import('../layouts/DefaultLayout.vue'),
    children: [
      {
        path: '',
        component: views('Home.vue'),
        meta: {
          middleware: [auth]
        }
      },
      {
        name: 'Contact',
        path: 'contact',
        component: views('Contact.vue'),
        meta: {
          middleware: [auth, isSubscriber]
        }
      },
      {
        path: 'blog',
        name: 'Blog',
        component: views('Blog.vue'),
        meta: {
          middleware: [guest]
        }
      },
      {
        path: 'chat',
        name: 'Chat',
        component: views('Chat.vue'),
        meta: {
          middleware: [guest]
        }
      },
      {
        path: 'about',
        name: 'About',
        component: views('About.vue'),
        children: [
          {
            name: 'About Me',
            path: 'me',
            component: views('AboutMe.vue'),
            meta: {
              middleware: [auth, isSubscriber]
            }
          }
        ]
      },
      {
        name: 'Other',
        path: '/other',
        component: views('Other.vue'),
        meta: {
          middleware: [guest]
        }
      }
    ]
  }
];
