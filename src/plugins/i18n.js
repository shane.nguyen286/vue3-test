import { createI18n } from 'vue-i18n';
import ja from '@/lang/ja.json';
import en from '@/lang/en.json';

const messages = {
  en,
  ja
};

const i18n = createI18n({
  locale: 'ja', // set locale
  fallbackLocale: 'en', // set fallback locale
  messages // set locale messages
});

export default i18n;
