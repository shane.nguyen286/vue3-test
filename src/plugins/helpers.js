export function increment(current, max = 10) {
  if (current < max) {
    return current + 1;
  }
  return current;
}

export const stringToCurrency = (money) => {
  if (!money) return 0;

  return parseFloat(money.replace(/,/g, ''));
};
