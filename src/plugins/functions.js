const AppFunctions = {
  install(app) {
    const App = app.config.globalProperties;
    App.$formatNumber = function (num) {
      return parseInt(num);
    };

    App.$formatTime = function (time) {
      return new Date(time);
    };

    // provide to App-level
    Object.keys(App).map((properties) => {
      app.provide(properties, App[properties]);
    });
  }
};

export default AppFunctions;
