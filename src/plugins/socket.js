import { io } from 'socket.io-client';

const socket = io('http://localhost:5000', {
  reconnectionDelay: 10000,
  auth: {
    token: '123'
  }
});

export default socket;
