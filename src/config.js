const AppConfig = {
  install(app) {
    // app.config.errorHandler = (err, instance, info) => {
    //   console.log(err, instance, info);
    // };
    // app.config.warnHandler = (msg, instance, trace) => {
    //   console.log(msg, instance, trace);
    // };
    app.config.compilerOptions.isCustomElement = (tag) => {
      return tag.startsWith('router-');
    };
  }
};

export default AppConfig;
