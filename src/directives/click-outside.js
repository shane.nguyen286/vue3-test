export default {
  beforeMount: function (el, binding) {
    const clickOutsideEvent = (event) => {
      if (!el.contains(event.target) && el !== event.target) {
        binding.value(event);
      }
    };

    el.__vueClickEventHandler__ = clickOutsideEvent;
    document.addEventListener('click', clickOutsideEvent);
  },
  unmounted: function (el) {
    document.body.removeEventListener('click', el.__vueClickEventHandler__);
  }
};
