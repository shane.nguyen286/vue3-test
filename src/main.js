import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import './assets/style/app.css';
import './assets/style/style.scss';

import store from './store';
import component from '@/plugins/component';
import AppFunctions from '@/plugins/functions';
import i18n from '@/plugins/i18n';
import Antd from 'ant-design-vue';
// import AppConfig from './config';

import clickOutside from '@/directives/click-outside';
import { MotionPlugin } from '@vueuse/motion';

import Datepicker from '@vuepic/vue-datepicker';
import '@vuepic/vue-datepicker/dist/main.css';
import 'ant-design-vue/dist/antd.css';
import VueLazyLoad from 'vue3-lazyload';

const app = createApp(App);

app.component('Datepicker', Datepicker);
app.directive('click-outside', clickOutside);
app.use(AppFunctions);
// app.use(AppConfig);
app.use(router).use(component).use(store).use(i18n);
app.use(MotionPlugin);
app.use(VueLazyLoad, {});
app.use(Antd);
app.mount('#app');
