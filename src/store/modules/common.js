export const state = {
  name: 'John',
  info: {
    name: 'Shane',
    age: 23,
    enough: false
  },
  data: [
    {
      title: 'One',
      checked: false
    },
    {
      title: 'Two',
      checked: true
    },
    {
      title: 'Three',
      checked: false
    }
  ],
  count: 1
};

export const getters = {
  name: (state) => state.name,
  info: (state) => state.info,
  data: (state) => state.data,
  count: (state) => state.count
};

export const mutations = {
  SET_NAME(state) {
    state.name = 'Shane';
  },
  increment(state) {
    state.count += 1;
  }
};

export const actions = {};
