[Vue3 Swiper Guide](https://programmer.group/using-swiper7-in-vue3.html)

### Vue Test Utils

- Use `mount()` to render a component.
- Use `get()` and `findAll()` to query the DOM.
- `trigger()` and `setValue()` are helpers to simulate user input.
- Updating the DOM is an `async` operation, so make sure to use `async` and `await`.
- Testing usually consists of 3 phases; `arrange`, `act` and `assert`.
- Use `find()` along with `exists()` to verify whether an element is in the DOM.
- Use `get()` if you expect the element to be in the DOM.
- The `data` mounting option can be used to set default values on a component.
- Use `get()` with `isVisible()` to verify the visibility of an element that is in the DOM
